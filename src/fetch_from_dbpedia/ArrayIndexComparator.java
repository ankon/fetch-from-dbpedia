/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fetch_from_dbpedia;

import java.util.Comparator;

/**
 *
 * @author TasnimAnkon
 */
public class ArrayIndexComparator implements Comparator<Integer> {
    
    private final String[] array;

    public ArrayIndexComparator(String[] array)
    {
        this.array = array;
    }

    public Integer[] createIndexArray()
    {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++)
        {
            indexes[i] = i; // Autoboxing
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //System.out.println(array[index1] + " " + array[index2]);
        //System.out.println(index1 + " " + index2 + " " + array[index1].compareTo(array[index2]));
        return array[index1].compareTo(array[index2]);
    }
    
}
