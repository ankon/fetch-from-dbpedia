/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fetch_from_dbpedia;

import java.util.Comparator;

/**
 *
 * @author TasnimAnkon
 */
public class IntegerArrayComparator implements Comparator<Integer>{
    
    private final int[] array;

    public IntegerArrayComparator(int[] array)
    {
        this.array = array;
    }

    public Integer[] createIndexArray()
    {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++)
        {
            indexes[i] = i; // Autoboxing
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        //return array[index1].compareTo(array[index2]);
        if (array[index1] < array[index2])
            return 1;
        else if (array[index1] == array[index2])
            return 0;
        else return -1;
    }
}
