/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fetch_from_dbpedia;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import com.cybozu.labs.langdetect.Language;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.Syntax;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.FileManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;


/**
 *
 * @author TasnimAnkon
 */
public class Fetch_from_dbpedia {

    /**
     * @param args the command line arguments
     */
    
    static SheetsQuickstart ob = new SheetsQuickstart();
    
    public static String[] finalProperties;
    public static String[] finalValues;
    public static String[] finalLanguages;
    
    public static void main(String[] args) {
        // TODO code application logic here
        sparqlTest();
    }
    
    static boolean isNumeric(String str) {
        try  
        {  
          double d = Double.parseDouble(str);  
        }  
        catch(NumberFormatException nfe)  
        {  
          return false;  
        }  
        return true;  
    }
    
    static void sparqlTest(){
        String[] englishProperty, englishValue, englishLanguage, frenchProperty, frenchValue, frenchLanguage;
        String[] frenchStatementType, englishStatementType, englishLocalName, frenchLocalName;
        FileManager.get().addLocatorClassLoader(Fetch_from_dbpedia.class.getClassLoader());
        
        String urlIdentifier = "", frenchUrlIdentifier = "";
        
        //TODO Initialize language detector
        String profileDirectory = "/Users/tasnimankonmanzur/Workspace/NetbeansProjects/Libraries/jar files/language-detection-master/profiles";
        Detector detector;
        String text;
        try {
            DetectorFactory.loadProfile(profileDirectory);
            
//            Detector detector = DetectorFactory.create();
//            String text = "lune";
//            detector.append(text);
//            System.out.println("Detected language: " + detector.detect());
//            
//            ArrayList<Language> langlist = detector.getProbabilities();
//            for (int i = langlist.size(); i > 0; i--) {
//                System.out.println("Probable language " + (langlist.size() - i) + ": " + langlist.get(langlist.size() - i));
//            }
        } catch (LangDetectException ex) {
            Logger.getLogger(Fetch_from_dbpedia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
//            ob.setName("চাঁদ");
//            ob.setName("সূর্য");
            ob.setName("পৃথিবী");
            urlIdentifier = ob.getEnglishName();
            frenchUrlIdentifier = ob.getFrenchName();
            
            ob.setName("Lune");
            System.out.println("French to English: " + ob.getEnglishFromFrenchName());
        } catch (IOException ex) {
            Logger.getLogger(Fetch_from_dbpedia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String modelUrl = "http://dbpedia.org/data/" + urlIdentifier + ".rdf";
        String objectUrl = "http://dbpedia.org/resource/" + urlIdentifier;
        
        String frenchModelUrl = "http://fr.dbpedia.org/data/" + urlIdentifier + ".rdf";
        String frenchObjectUrl = "http://fr.dbpedia.org/resource/" + urlIdentifier;
        
        int numberOfProperties = 0;
        int frenchProperties = 0;
        
        Model model = FileManager.get().loadModel(modelUrl);
        
        String queryString = "SELECT ?p ?o " +
                        "WHERE " +
                        "  {" +
                        "     <" + objectUrl + ">  ?p  ?o ." +
                        "  }";
        
        /*String queryString = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
                        + "SELECT ?p " +
                        "WHERE " +
                        "  {" +
                        "     <http://dbpedia.org/resource/Ansungtangmyun> rdf:Property  ?p" +
                        "  }";
        
        String queryString = "SELECT ?p " +
                        "WHERE " +
                        "  {" +
                        "     <http://dbpedia.org/resource/Ansungtangmyun> <http://www.w3.org/1999/02/22-rdf-syntax-ns#hasProperty>  ?p" +
                        "  }";*/
        Query query = QueryFactory.create(queryString);
        QueryExecution qexec = QueryExecutionFactory.create(query, model);
        englishProperty = new String[1000];
        englishValue = new String[1000];
        englishLanguage = new String[1000];
        englishStatementType = new String[1000];
        try{
            ResultSet results = qexec.execSelect();
            while(results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                //Literal tag = soln.getLiteral("p");
                //org.apache.jena.rdf.model.impl.ResourceImpl tag = (org.apache.jena.rdf.model.impl.ResourceImpl) soln.get("p");
                RDFNode property = (Resource) soln.getResource("p");
                RDFNode object = soln.get("o");
                
//                System.out.println(numberOfProperties + ". " + property.asResource().getURI().toString());
                englishProperty[numberOfProperties] = property.asNode().getLocalName().toString();
//                System.out.println(numberOfProperties + " " + property.asResource().getURI().toString());
                
                if (object != null) {
                    String value = "";
                    if (object.isLiteral()) {
                        value = object.asLiteral().getLexicalForm();
                        //System.out.println(object.asLiteral().getLanguage());
                        englishLanguage[numberOfProperties] = object.asLiteral().getLanguage();
                        englishStatementType[numberOfProperties] = "literal";
//                        System.out.println(englishProperty[numberOfProperties] + "Lexical name: " + value);
                    } else {
                        value = object.asNode().getLocalName();
//                        System.out.println(englishProperty[numberOfProperties] + "Local name: " + value + " " + object.asNode().getURI());
                        if (value.isEmpty()) {
                            value = object.asNode().getURI();
//                            System.out.println(englishProperty[numberOfProperties] + "URI name: " + value);
                        }
                        englishStatementType[numberOfProperties] = "uri";
                    }
                    //System.out.println("value: " + value + "\n");
                    englishValue[numberOfProperties] = value;
                }
                numberOfProperties++;
            }
        }finally{
            qexec.close();
            System.out.println("\n\nTotal number of properties : " + String.valueOf(numberOfProperties - 1) +  "\n\n");
        }
        
        Model FrenchModel = FileManager.get().loadModel(frenchModelUrl);
        
        queryString = "SELECT ?p ?o " +
                        "WHERE " +
                        "  {" +
                        "     <" + frenchObjectUrl + ">  ?p  ?o " +
                        "  }";
        
        Query FrecnchQuery = QueryFactory.create(queryString);
        QueryExecution FrenchQExec = QueryExecutionFactory.create(FrecnchQuery, FrenchModel);
        try{
            ResultSet results = FrenchQExec.execSelect();
            frenchProperty = new String[1000];
            frenchValue = new String[1000];
            frenchLanguage = new String[1000];
            frenchStatementType = new String[1000];
            while(results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                //Literal tag = soln.getLiteral("p");
                //org.apache.jena.rdf.model.impl.ResourceImpl tag = (org.apache.jena.rdf.model.impl.ResourceImpl) soln.get("p");
                RDFNode property = (Resource) soln.getResource("p");
                RDFNode object = soln.get("o");
//                System.out.println(frenchProperties + ". " + property.asNode().getLocalName().toString());// + " value: " + object.asNode().getLocalName());
                frenchProperty[frenchProperties] = property.asNode().getLocalName().toString();
//                System.out.println(frenchProperties + " " + property.asResource().getURI().toString());
                
                if (object != null) {
                    String value = "";
                    if (object.isLiteral()) {
                        value = object.asLiteral().getLexicalForm();
                        frenchLanguage[frenchProperties] = object.asLiteral().getLanguage();
                        frenchStatementType[frenchProperties] = "literal";
                        
                        if (frenchLanguage[frenchProperties] == null || frenchLanguage[frenchProperties].isEmpty()) {
                            try {
                                //value = object.asNode().getLocalName();
                                detector = DetectorFactory.create();
                                text = value;
    //                            System.out.println(text);
                                if (text != null && !text.isEmpty()) {
                                    if (isNumeric(text)) {
                                        frenchLanguage[frenchProperties] = "en";
                                    } else {
                                        detector.append(text);

                                        ArrayList<Language> langlist = detector.getProbabilities();
                                        for (int i = langlist.size(); i > 0; i--) {
//                                          System.out.println("Probable language " + (langlist.size() - i) + ": " + langlist.get(langlist.size() - i));
                                            if (langlist.get(langlist.size() - i).toString().contains("fr")) {
                                                frenchLanguage[frenchProperties] = "fr";
                                                break;
                                            }
                                        }
                                    }
                                }
                            } catch (LangDetectException ex) {
                                Logger.getLogger(Fetch_from_dbpedia.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    } else {
                        try {
                            value = object.asNode().getLocalName();
                            detector = DetectorFactory.create();
                            text = value.toString();
//                            System.out.println(text);
                            if (text != null && !text.isEmpty()) {
                                text = text.replaceAll("_", " ");
//                                System.out.println(text);
                                detector.append(text);

                                ArrayList<Language> langlist = detector.getProbabilities();
                                for (int i = langlist.size(); i > 0; i--) {
    //                                System.out.println("Probable language " + (langlist.size() - i) + ": " + langlist.get(langlist.size() - i));
                                    if (langlist.get(langlist.size() - i).toString().contains("fr")) {
                                        frenchLanguage[frenchProperties] = "fr";
                                        break;
                                    }
                                }
                            }
                        } catch (LangDetectException ex) {
                            Logger.getLogger(Fetch_from_dbpedia.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                        if (value.isEmpty()) {
                            value = object.asNode().getURI();
                            
                            if (frenchLanguage[frenchProperties] == null || frenchLanguage[frenchProperties].isEmpty()) {
                                if (value.toString().contains("fr.dbpedia.org")) {
                                    frenchLanguage[frenchProperties] = "fr";
                                } else if (value.toString().contains("fr.wikipedia.org")) {
                                    frenchLanguage[frenchProperties] = "fr";
                                }
                            }
                        }
                        
                        frenchStatementType[frenchProperties] = "uri";
                    }
//                    System.out.println("value: " + frenchLanguage[frenchProperties]);
                    frenchValue[frenchProperties] = value;
                }
                
                frenchProperties++;
            }
        }finally{
            qexec.close();
            System.out.println("Total number of French properties : " + String.valueOf(frenchProperties - 1));
        }
        
        String[] combinedProperty = new String[numberOfProperties + frenchProperties + 1];
        String[] combinedValue = new String[numberOfProperties + frenchProperties + 1];
        String[] combinedLanguage = new String[numberOfProperties + frenchProperties + 1];
        String[] combinedStatementType = new String[numberOfProperties + frenchProperties + 1];
        
        int i,j;
        String iteratedProperty, iteratedValue, iteratedLanguage, iteratedStatmentType;
        String temporaryValue;
        boolean flag;
        
        for (i = 0; i < numberOfProperties; i++) {
            combinedProperty[i] = englishProperty[i];
            combinedValue[i] = englishValue[i];
            combinedLanguage[i] = englishLanguage[i];
            combinedStatementType[i] = englishStatementType[i];
        }
        
        int newProperties = 0;
        
        for (i = 0; i < frenchProperties; i++) {
            flag = false;
            iteratedProperty = frenchProperty[i];
            iteratedValue = frenchValue[i];
            iteratedLanguage = frenchLanguage[i];
            iteratedStatmentType = frenchStatementType[i];
            for (j = 0; j < numberOfProperties; j++) {
                if (combinedProperty[j].equals(iteratedProperty)) {
                    if (combinedValue[j].equals(iteratedValue)) {
                        if (iteratedLanguage != null && !iteratedLanguage.isEmpty() && combinedLanguage[j] != null && !combinedLanguage[j].isEmpty()) {
                            if (combinedLanguage[j].equals(iteratedLanguage)) {
                                flag = true;
                                break;
                            }
                        } else {
                            flag = true;
                            break;
                        }
                        //TODO detect language, translate and modify property
                        flag = true;
                        break;
                    }
                }
            }
            
//            System.out.println(iteratedLanguage + " " + iteratedStatmentType + " " + iteratedValue);
            
            if (iteratedLanguage != null && iteratedLanguage.equals("fr") && iteratedStatmentType != null && iteratedStatmentType.equals("literal") && !iteratedValue.isEmpty() && !isNumeric(iteratedValue)) {
                try {
                    ob.setName(iteratedValue);
//                    System.out.println("French to English: " + ob.getEnglishFromFrenchName());
                    temporaryValue = ob.getEnglishFromFrenchName();
                    
                    for (j = 0; j < numberOfProperties; j++) {
                        if (combinedValue[j].equals(temporaryValue)) {
                            iteratedProperty = combinedProperty[j];
                            iteratedLanguage = "fr";
                            System.out.println("Modified: " + iteratedProperty + " " + iteratedValue + " " + iteratedLanguage);
                            flag = false;
                            break;
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Fetch_from_dbpedia.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if (iteratedLanguage != null && iteratedLanguage.equals("fr") && iteratedStatmentType != null && iteratedStatmentType.equals("uri") && !iteratedValue.isEmpty() && !isNumeric(iteratedValue)) {
                for (j = 0; j < numberOfProperties; j++) {
                    if (combinedValue[j].equals(iteratedValue) && combinedStatementType[j] != null && combinedStatementType[j].equals("uri")) {
                        if (combinedLanguage[j] == null || !combinedLanguage[j].equals("fr")) {
                            iteratedProperty = combinedProperty[j];
                            iteratedLanguage = "fr";
                            flag = false;
//                            System.out.println("Modified: " + iteratedProperty + " " + iteratedValue + " " + iteratedLanguage);
                            break;
                        }
                    }
                }
            }
            
            if (!flag) { 
                combinedProperty[numberOfProperties + newProperties] = iteratedProperty;
                combinedValue[numberOfProperties + newProperties] = iteratedValue;
                combinedLanguage[numberOfProperties + newProperties] = iteratedLanguage; 
                newProperties++;
            } else {
//                System.out.println("\nCommon Property: " + iteratedProperty + " " + iteratedValue + " " + iteratedLanguage);
            }
        }
        
        System.out.println("\n\nTotal Properties: " + String.valueOf(numberOfProperties + newProperties) + "\n\n");
        
        ////////////////////Alphabetical sort of Properties////////////////////
        
        String[] totalProperties = new String[numberOfProperties + newProperties];
        String[] totalValues = new String[numberOfProperties + newProperties];
        String[] totalLanguages = new String[numberOfProperties + newProperties];
        
        for (i = 0; i < (numberOfProperties + newProperties); i++) {
//            System.out.println((i + 1) + ". Property: " + combinedProperty[i] + " Language: " + combinedLanguage[i] + " \nValue: " + combinedValue[i] + "\n");
            totalProperties[i] = combinedProperty[i];
            totalValues[i] = combinedValue[i];
            totalLanguages[i] = combinedLanguage[i];
        }
        
        ArrayIndexComparator comparator = new ArrayIndexComparator(totalProperties);
        Integer[] indexes = comparator.createIndexArray();
        Arrays.sort(indexes, comparator);
        
        int traversingIndex;
        
        finalProperties = new String[numberOfProperties + newProperties];
        finalValues = new String[numberOfProperties + newProperties];
        finalLanguages = new String[numberOfProperties + newProperties];
        int finalNumber = indexes.length;
        
        for (i = 0; i < indexes.length; i++) {
            traversingIndex = indexes[i];
            finalProperties[i] = totalProperties[traversingIndex];
            finalValues[i] = totalValues[traversingIndex];
            finalLanguages[i] = totalLanguages[traversingIndex];
//            System.out.println((i + 1) + ". Property: " + totalProperties[traversingIndex] + " Language: " + totalLanguages[traversingIndex] + " \nValue: " + totalValues[traversingIndex] + "\n");
        }
        
        ////////////////Fetch Language of URLs///////////////////////
//        String language = "";
//        String[] schemes = {"http","https"}; // DEFAULT schemes = "http", "https", "ftp"
//        UrlValidator urlValidator = new UrlValidator(schemes);
//        
//        for (i = 0; i < indexes.length; i++) {
//            if(finalLanguages[i] == null && urlValidator.isValid(finalValues[i])) {
//                language = GetHTMLContent(finalValues[i]);
//                finalLanguages[i] = language;
//                System.out.println(language);
//            }
//        }
        /////////////////////////////////////////////////////////////
        
        //////////////////sort basing on content length//////////////////////////
        int startValue = 0, finalValue = 0, consecutiveValue = 1;
        boolean referenceValue = true;
        String referenceProperty = finalProperties[0];
        int[] contentLengths;
        
        String[] backupProperties = new String[finalProperties.length];
        String[] backupValues = new String[finalValues.length];
        String[] backupLanguages = new String[finalLanguages.length];
        
        for (i = 0; i < finalProperties.length; i++) {
            backupProperties[i] = finalProperties[i];
            backupValues[i] = finalValues[i];
            backupLanguages[i] = finalLanguages[i];
        }
        
        int PropertiesBasedOnLength = 0;
        
        for (i = 1; i < indexes.length; i++) {
            if (referenceProperty.equals(backupProperties[i])) {
                finalValue++;
                consecutiveValue++;
            } else {
                finalValue = i - 1;
                referenceValue = false;
            }
            
            if (!referenceValue) {
                contentLengths = new int[consecutiveValue];
                
                for (j = 0; j < (consecutiveValue); j++) {
                    contentLengths[j] = backupValues[j + startValue].length();
                }
                
                //System.out.println(referenceProperty + " " + consecutiveValue + " Lengths: " + Arrays.toString(contentLengths));
                
                ////////////////////Sorting Algorithm/////////////////////
                IntegerArrayComparator intComparator = new IntegerArrayComparator(contentLengths);
                Integer[] integerIndexes = intComparator.createIndexArray();
                Arrays.sort(integerIndexes, intComparator);
                
                int sortedTraversingIndex;
                
                //System.out.print(referenceProperty + " " + consecutiveValue + " Sorted Lengths: " );
                
                for (j = 0; j < consecutiveValue; j++) {
                    sortedTraversingIndex = integerIndexes[j];
                    finalProperties[startValue + j] = backupProperties[startValue + sortedTraversingIndex];
                    finalValues[startValue + j] = backupValues[startValue + sortedTraversingIndex];
                    finalLanguages[startValue + j] = backupLanguages[startValue + sortedTraversingIndex];
                    //System.out.print(contentLengths[sortedTraversingIndex] + " ");
                }
                //System.out.print("\n");
                //////////////////////////////////////////////////////////
                
                PropertiesBasedOnLength += consecutiveValue;
                
                startValue = i;
                finalValue = i;
                consecutiveValue = 1;
                referenceValue = true;
                referenceProperty = backupProperties[i];
            }
            
            if (i == indexes.length - 1) {
                contentLengths = new int[consecutiveValue];
                
                for (j = 0; j < (consecutiveValue); j++) {
                    contentLengths[j] = finalValues[j + startValue].length();
                }
                
                //System.out.println(referenceProperty + " " + consecutiveValue + " Lengths: " + Arrays.toString(contentLengths));
                
                ////////////////////Sorting Algorithm for Last Property/////////////////////
                IntegerArrayComparator intComparator = new IntegerArrayComparator(contentLengths);
                Integer[] integerIndexes = intComparator.createIndexArray();
                Arrays.sort(integerIndexes, intComparator);
                
                int sortedTraversingIndex;
                
                //System.out.print(referenceProperty + " " + consecutiveValue + " Sorted Lengths: " );
                
                for (j = 0; j < consecutiveValue; j++) {
                    sortedTraversingIndex = integerIndexes[j];
                    finalProperties[startValue + j] = backupProperties[startValue + sortedTraversingIndex];
                    finalValues[startValue + j] = backupValues[startValue + sortedTraversingIndex];
                    finalLanguages[startValue + j] = backupLanguages[startValue + sortedTraversingIndex];
                    //System.out.print(contentLengths[sortedTraversingIndex] + " ");
                }
                //System.out.print("\n");
                ////////////////////////////////////////////////////////////////////////////////
                
                PropertiesBasedOnLength += consecutiveValue;
            }
        }
        
        System.out.println("\n\nProperties based on length: " + PropertiesBasedOnLength + "\n\n");
        /////////////////////////////////////////////////////////////////////////
        
        ///////////////////////Print//////////////////////
//        for (i = 0; i < finalNumber; i++) { 
//            System.out.println((i + 1) + ". Property: " + finalProperties[i] + " Lang: " + finalLanguages[i] + "\nValue: " + finalValues[i] + "\n");
//        }
        //////////////////////////////////////////////////
        
        //GetHTMLContent("http://stackoverflow.com/");
    }
    
    public static String GetHTMLContent (String Url) {
        ////////////////check language of link////////////////////
        
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        String htmlContent = "";

        try {
            //url = new URL("http://stackoverflow.com/");
            url = new URL(Url);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while ((line = br.readLine()) != null) {
                htmlContent = htmlContent + line;
                //System.out.println(line);
            }
        } catch (MalformedURLException mue) {
             mue.printStackTrace();
             System.out.println("Malformed URL");
             return "";
        } catch (IOException ioe) {
             //ioe.printStackTrace();
             System.out.println("IO Exception");
             return "";
        } finally {
            try {
                if (is != null) is.close();
            } catch (IOException ioe) {
                // nothing to see here
                ioe.printStackTrace();
                System.out.println("2nd IO Exception");
                return "";
            }
        }
        
        //System.out.println(htmlContent);
        
        org.jsoup.nodes.Document htmlDoc = Jsoup.parse(htmlContent);
        
        //System.out.println("Parsed value: " + htmlDoc.select("html").first().toString());
        
        Element taglang = htmlDoc.select("html").first();
        
        //System.out.println("Lang: " + taglang.attr("lang"));
        
        return taglang.attr("lang");
        
        //////////////////////////////////////////////////////////
    }
    
}
